Eine Anwendung zum Laden von BIM-Profilen aus der BIMeta-API zur Anzeige auf der Seite des CAFM-Ring (https://www.cafm-connect.org/bim-profile/).

Features:
- Schnelles Einladen der Profile
- Filterung nach Namen und weiteren Parametern
- Detailansicht von Klassen und Merkmalen

Geplant:
- Export von Profilen (inklusive der Klassen und Merkmale)
